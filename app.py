from flask import Flask, render_template
import argparse

app = Flask(__name__)
app.secret_key = 'super secret key'

@app.route('/', methods=['GET','POST'])
def main():
    return render_template('index.html')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', default=4000)
    args = parser.parse_args()

    app.run(host='0.0.0.0', port=args.port)